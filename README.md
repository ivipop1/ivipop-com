<img alt="ivipop" src="https://github.com/Ivipop/ivipop.com/blob/main/img/ivipop-github.png?raw=true" style="max-width: 100%;">

<h1>Plateforme de partage d'évènements</h1>
<h2>Ivipop.com</h2>

<a href="http://creativecommons.org/licenses/by-nc-nd/4.0/">
<img alt="Licence Creative Commons" src="https://camo.githubusercontent.com/1eb7683341794b302bcd8741de5cfb3c4f4b58c879f69d0e01a57c1e255844d9/68747470733a2f2f692e6372656174697665636f6d6d6f6e732e6f72672f6c2f62792d6e632d6e642f342e302f38307831352e706e67" style="max-width: 100%;"></a>

<p>Ivipop est une plateforme de partage et de recherche d'evenement open source en France et en Europe.</p>
<p>Nous developpons notre moteur de recherche dans le respect des utilisateurs et des organisateurs</p>
